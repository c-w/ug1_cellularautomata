# Coords :: (Int, Int)
# Entry  :: (Coords, Float, Boolean)
# Row    :: [ Entry ]
# Board  :: [ Row ]

# toString :: Board   ->   String
def toString(board):
	
	"""	toString(board)
	returns String representation of board"""
	
	out = ""
	
	for row in board:
		for ((r,c),v,b) in row:
			out += ("%.4f\t" % v)
		out += ("\n")
	
	return out


# initBoard :: Int, Int, Float   ->   Board
def initBoard(rows=10, cols=10, defaultValue=0.5):
	
	"""	initBoard(rows=10, cols=10, defaultValue=0.5)
	returns a list of lists of ((row,col),value,editable=True) tuples
	(each list is a row in the board)"""
	
	return [ [ ((r,c),defaultValue,True) for c in range(cols) ] for r in range(rows) ]


# setConstantValue :: Coords, Float, Board   ->   Board
def setConstantValue((row, col), val, board):
	
	""" setConstantValue((row, col), val, board)
	sets the element with coordinates (row, col) in board to val and marks it non-changing"""
	
	try:
		editedRow = [ ((r,c),val,False) if c == col else ((r,c),v,b) for ((r,c),v,b) in board[row] ]
	except IndexError:
		return board
	
	return [ editedRow if i == row else board[i] for i in range(len(board)) ]


# mooreNeighbourhoodAndSelf :: Coords, Board   ->   [ Entry ]
def mooreNeighbourhoodAndSelf((row, col), board):
	
	""" mooreNeighbourhoodAndSelf((row, col), board)
	creates list of elements in wraped Moore-neighbourhood of (row,col) and itself"""
	
	yMax = len(board)
	xMax = len(board[0])
	
	return [ board[(row+dy)%yMax][(col+dx)%xMax]	for dy in range(-1,2)
													for dx in range(-1,2) ]


# makeAverage :: Coords, Board, f(Coords, Board -> [Entry])   ->   Float
def makeAverage((row, col), board, neighbourhoodFunction):
	
	"""	makeAverage((row, col), board, neighbourhoodFunction)
	returns average of values of elements in neighbourhood of element at (row,col) defined by neighbourhoodFunction and itself"""
	
	nHood = neighbourhoodFunction((row, col), board)
	
	return sum(map(lambda ((r,c),v,b): v, nHood))/len(nHood)


# mooreAverage :: Board   ->   Board
def mooreAverage(board):
	
	"""	mooreAverage(board)
	calls makeAverage(mooreNeighbourhoodAndSelf) on each changeable entry in board and returns new board"""
	
	return [ map(lambda ((r,c),v,b): ((r,c),makeAverage((r,c),board,mooreNeighbourhoodAndSelf),b) if b else ((r,c),v,b), row) for row in board ]


# iterate :: Board, int, f(Board -> Board)   ->   Board
def iterate(board, iterationRules=mooreAverage):
	
	"""	iterate(board, iterationRules=mooreAverage)
	calls iterationRules() on board and returns new board"""
	
	return iterationRules(board)