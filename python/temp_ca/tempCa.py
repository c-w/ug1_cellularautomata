import os, sys, getopt, re
import pygame, pygame._view
import cellularAutomaton

def usage():
	
	"""	usage()
	write usage message to stderr"""
	
	sys.stderr.write( "Simulates temperature spreading using a cellular automaton model.\n\t \
	Usage: %s [OPTIONS]\n\t \
		-h shows this usage message\n\t \
		-r height of simulated field (default 10)\n\t \
		-c width of simulated field (default 10)\n\t \
		-v starting value of all elements in field (default 0.5)\n\t \
		-s (x,y,v) sets field at x,y to value v and marks it constant (can invoke multiple times)\n\
	" % (sys.argv[0]) )


def main(argv):
	
	"""	main(argv)
	connect command-line parameters, pygame GUI, and cellularAutomaton module"""
	
	tileSize = 10
	rows = 10
	cols = 10
	val  = 0.5
	constants = []
	
	try:
		opts, args = getopt.getopt(argv, "hr:c:v:s:", ["help", "rows=", "columns=", "value=", "set="])
	except getopt.GetoptError:
		usage()
		sys.exit(2)
	
	for (opt, arg) in opts:
		if opt in ("-h", "--help"):
			usage()
			sys.exit()
		elif opt in ("-r", "--rows"):
			rows = int(arg)
		elif opt in ("-c", "--columns"):
			cols = int(arg)
		elif opt in ("-v", "--value"):
			val = float(arg)
		elif opt in ("-s", "--set"):
			if re.match("^\((\d+),(\d+),(\d+(\.\d*)?|\.\d+)\)$", arg):
				constants.append((lambda (x,y,v): (int(x),int(y),float(v)))(tuple(arg[1:len(arg)-1].split(','))))
			
	board = cellularAutomaton.initBoard(rows,cols,val)
	
	for (x,y,v) in constants:
		if v >= 0.0 and v <= 1.0:
			board = cellularAutomaton.setConstantValue((x,y),v,board)
	
	pygame.init()
	screen = pygame.display.set_mode((cols*tileSize, rows*tileSize))
	pygame.display.set_caption("Temperature Cellular Automaton")
	screen.fill((255/2,255/2,255/2))
	
	running = 1
	loops = 0
	while(running):
		loops += 1
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				running = 0
		
		for row in board:
			for ((y,x),v,b) in row:
					rct = (x*tileSize, y*tileSize, tileSize, tileSize)
					col = ((1-v)*255, 0, v*255)
					pygame.draw.rect(screen, col, rct)
		
		pygame.display.flip()
		if loops%100 == 0:
			board = cellularAutomaton.iterate(board)


if __name__ == "__main__":
	main(sys.argv[1:])