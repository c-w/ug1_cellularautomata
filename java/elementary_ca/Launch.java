package elementary_ca;

import jgame.*;
import jgame.platform.*;

import elementary_ca.frontend.*;
import elementary_ca.backend.*;
import elementary_ca.utils.*;

public class Launch extends JGEngine
{
	private boolean isRun;
	private boolean isRandom;
	private Button[] buttons;
	private Board board;
	private int[] pattern;
	
	public final static int FRAMESKIP = 2;
	public final static int FPS = 100;
	public final static int WIDTH         = 600;
	public final static int HEIGHT        = 600;
	public final static int TILE_SIZE     =   3;
	public final static int FOOTER_HEIGHT =  75;
	public final static int FOOTER_X      =   0;
	public final static int FOOTER_Y      = HEIGHT-FOOTER_HEIGHT;
	public final static int BUTTON_X      = FOOTER_X + Button.WIDTH;
	public final static int BUTTON_Y      = HEIGHT - 2*Button.HEIGHT;
	public final static int BUTTON_DX = (WIDTH-Button.NAMES.length*Button.WIDTH)/(Button.NAMES.length+1);
	public final static int TILES_H   = WIDTH/TILE_SIZE;
	public final static int TILES_V   = HEIGHT/TILE_SIZE;
	
	public final static int ROWS      = TILES_V - FOOTER_HEIGHT/TILE_SIZE;
	public final static int RULES_NUM = 8;
	
	public final static JGColor FG_COL = new JGColor(51, 153, 51);
	public final static JGColor BG_COL = new JGColor(25,  25, 25);
	public final static JGFont FONT = new JGFont("monosp", JGFont.BOLD, 250.0);
	
	public final static int MOUSE_LEFT  =  1;
	public final static int MOUSE_RIGHT =  3;
	public final static int BACKSPACE   =  8;
	public final static int CTRL        = 17;
	public final static int SPACE_BAR   = 32;
	
	public final static int CELL_CID    = 1;
	
	public static void main(String[] args)
	{
		new Launch(new JGPoint(WIDTH, HEIGHT));
	}
		
	public Launch() // start as applet
	{
		initEngineApplet();
	}
	
	public Launch(JGPoint size) // start as jar
	{
		initEngine(size.x, size.y);
	}
	
	public void initCanvas()
	{
		setCanvasSettings(
				TILES_H,   // number of horizontal tiles
				TILES_V,   // number of vertical tiles
				TILE_SIZE, // width of one tile
				TILE_SIZE, // height of one tile
				FG_COL,    // foreground color (null = default/white)
				BG_COL,    // background color (null = default/black)
				FONT       // font             (null = default)
			);
	}
	
	
	
	/* ################ Initialize Game ################ */
	
	public void initGame()
	{
		setProgressMessage("Elementary Cellular Automaton");
		setAuthorMessage("Loading...");
		defineMedia("media.tbl"); // pointer to media declarations
		setFrameRate(FPS, FRAMESKIP);
		setBGImage("bg_default");
		
		removeObjects(null, 0);
		isRun        = false;
		buttons      = new Button[Button.NAMES.length+1];
		pattern      = new int[RULES_NUM];
		isRandom     = false;
		
		int i;
		
		for (i = 0; i < Button.NAMES.length; i++)
		{
			buttons[i] = new Button(BUTTON_X + i*(Button.WIDTH+BUTTON_DX), BUTTON_Y, Button.NAMES[i]);
		}
		
		setGameState("Setup");
	}
	
	
	
	/* ################ GameState: Setup ################ */
	
	public void startSetup() // called once when state is entered
	{
		setMouseCursor(DEFAULT_CURSOR);
	}
	
	public void doFrameSetup() // called at each tick while in state
	{
		checkButtons();
		clearBoard();
		switchState();
	}
	
	
	
	/* ################ GameState: Run ################ */
	
	public void startRun() // called once when state is entered
	{
		setMouseCursor(WAIT_CURSOR);
		removeObjects(null, CELL_CID);
		buildBoard();
	}
	
	public void doFrameRun() // called at each tick while in state
	{
		checkDone();
		drawNextGen();
		switchState();
	}
	
	
	
	/* ################ Shared Functions ################ */
	
	public void switchState()
	{
		if (getKey(SPACE_BAR))
		{
			clearKey(SPACE_BAR);
			nextState();
		}
	}
	
	
	
	/* ################ Setup Functions ################ */
	
	public void checkButtons()
	{
		int i;
		
		for (i = 0; i < Button.NAMES.length; i++)
		{
			if (buttons[i].hover(getMouseX(), getMouseY()))
			{
				if (getMouseButton(MOUSE_LEFT))
				{
					clearMouseButton(MOUSE_LEFT);
					buttons[i].toggle();
					
					if (i < RULES_NUM)
					{
						if (buttons[i].isOn()) { pattern[i] = Board.FULL;  }
						else                   { pattern[i] = Board.EMPTY; }
					}
					else
					{
						isRandom = buttons[i].isOn();
					}
				}
			}
		}
	}
	
	public void clearBoard()
	{
		if (getKey(BACKSPACE))
		{
			clearKey(BACKSPACE);
			removeObjects(null, CELL_CID); // remove all cells
		}
	}
	
	
	
	/* ################ Run Functions ################ */
	
	public void buildBoard()
	{
		board = new Board(TILES_H, pattern);
		if (isRandom) { board.loadRandomStartingGen(); }
		else          { board.loadSimpleStartingGen(); }
		drawGen();
	}
	
	public void drawNextGen()
	{
		board.makeNextGen();
		drawGen();
	}
	
	public void checkDone()
	{
		if (board.getGenNo()+1 == ROWS)
		{
			nextState();
		}
	}
	
	
	
	/* ################ Minor Functions ################ */
	
	private void drawGen()
	{
		int i;
		int line  = board.getGenNo();
		int[] gen = board.getCurGen();
			
		for (i = 0; i < gen.length; i++)
		{
			if (gen[i] == Board.FULL)
			{
				new Cell(i*TILE_SIZE, line*TILE_SIZE, CELL_CID);
			}
		}
	}
	
	private void nextState()
	{
		if (isRun)
		{
			setGameState("Setup");
		}
		else
		{
			setGameState("Run");
		}
		isRun = !isRun;
	}
}