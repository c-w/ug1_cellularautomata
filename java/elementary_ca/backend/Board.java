package elementary_ca.backend;

import elementary_ca.utils.IntArrayUtils;

public class Board
{
	private static int DEFAULT_NUMGENS     =  5;
	private static int DEFAULT_WIDTH       = 10;
	private static boolean DEFAULT_RANDOM  = false;
	private static int[] DEFAULT_RULES = new int[] {0,1,1,0,1,1,1,0};
	
	public static int EMPTY = 0;
	public static int FULL  = 1;
	
	private int[] rules;  // rules[7] <- 111
	                      // rules[6] <- 110
						  // rules[5] <- 101
						  // rules[4] <- 100
						  // rules[3] <- 011
						  // rules[2] <- 010
						  // rules[1] <- 001
						  // rules[0] <- 000
	private int[] curGen;
	private int width;
	private int genNo;
	
	public Board(int w, int[] pattern)
	{
		this.width  = w;
		this.genNo  = 0;
		this.curGen = new int[width];
		this.rules  = pattern;
	}
	
	public Board(int[] startingBoard, int[] pattern)
	{
		this.width  = startingBoard.length;
		this.genNo  = 0;
		this.curGen = IntArrayUtils.copy(startingBoard);
		this.rules  = pattern;
	}
	
	public String toString()
	{
		int i;
		String generationNum = "(" + Integer.toString(genNo) + ")\t";
		String generationRep = new String();
		
		for (i = 0; i < width; i++)
		{
			generationRep = generationRep + Integer.toString(curGen[i]) + " ";
		}
		generationRep = generationRep + "\n";
		
		return (generationNum + generationRep);
	}
	
	public int[] getCurGen() { return curGen; }
	public int getGenNo()    { return genNo;  }
	
	public void loadSimpleStartingGen()
	{
		curGen[width/2] = 1;
	}
	
	public void loadRandomStartingGen()
	{
		int i;
		
		for (i = 0; i < width; i++)
		{
			if (Math.random() < 0.5) { curGen[i] = 1; }
		}
	}
	
	public void makeNextGen()
	{
		curGen = iterate(curGen, rules);
		genNo++;
	}
	
	
	
	private int[] iterate(int[] old, int[] pattern)
	{
		int i;
		int[] nextGen = new int[old.length];
		
		for (i = 0; i < nextGen.length; i++)
		{
			nextGen[i] = applyPattern(i, old, pattern);
		}
		
		return nextGen;
	}
	
	private int applyPattern(int cell, int[] a, int[] pattern) // wraps around
	{
		if      (cell == 0)          { return pattern[ a[a.length-1]*4 + a[cell]*2 + a[cell+1]*1       ]; }
		else if (cell == a.length-1) { return pattern[ a[cell-1]*4     + a[cell]*2 + a[a.length-1]*1   ]; }
		else                         { return pattern[ a[cell-1]*4     + a[cell]*2 + a[cell+1]*1       ]; }
	}
	
	private static int[] makeRule(String s)
	{
		return IntArrayUtils.parseCSV(s);
	}
	
	
	
	public static void main(String[] args)
	{
		int argc = args.length;
		int numGens;
		int[] rules;
		int[] board;
		boolean random = DEFAULT_RANDOM;
		Board b;
		int i;
		
		
		
		/* #### argument handling #### */
		if (argc == 0)
		{
			System.out.printf("tests: Board numGens(int) random(y/n) rules(int,int,...) startingBoard(int,int,...)\n");
			System.out.printf("       using default values for now\n\n");
		}
		
		numGens = (argc >= 1) ? Integer.parseInt(args[0]) : DEFAULT_NUMGENS;
		
		if (argc >= 2)
		{
			if (args[1].length() != 1)
			{
				System.out.printf("error: random flag must match y or n\n");
				System.out.printf("       using default value (n) for now\n\n");
			}
			else
			{
				random = args[1].equals("y");
			}
		}
		else
		{
			random = DEFAULT_RANDOM;
		}
		
		if (argc >= 3)
		{
			if (args[2].length() != 15)
			{
				System.out.printf("error: rules must match int,int,int,int,int,int,int,int\n");
				System.out.printf("       using default rules for now\n\n");
				rules = DEFAULT_RULES;
			}
			else
			{
				rules = makeRule(args[2]);
			}
		}
		else
		{
			rules = DEFAULT_RULES;
		}
		
		if (argc >= 4)
		{
			b = new Board(IntArrayUtils.parseCSV(args[3]), rules);
		}
		else
		{
			b = new Board(DEFAULT_WIDTH, rules);
			
			if (random)
			{
				b.loadRandomStartingGen();
			}
			else
			{
				b.loadSimpleStartingGen();
			}
		}
		/* #### argument handling end #### */
		
		
		
		for (i = 0; i <= numGens; i++)
		{
			System.out.printf("%s", b.toString());
			b.makeNextGen();
		}
	}
}