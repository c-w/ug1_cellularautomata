package elementary_ca.utils;

public class IntArrayUtils
{
	public static int[] reverse(int[] in)
	{
		int i; int j;
		int[] out = new int[in.length];
		
		for (i = in.length-1, j = 0; i >= 0; i--, j++)
		{
			out[j] = in[i];
		}
		
		return out;
	}
	
	public static int[] copy(int[] a)
	{
		return ((int[]) a.clone());
	}
	
	public static int[] parseCSV(String s)
	{
		int i;
		String[] tokens = s.split(",");
		int[] out = new int[tokens.length];
		
		for (i = 0; i < out.length; i++)
		{
			out[i] = Integer.parseInt(tokens[i]);
		}
		
		return out;
	}
}