package elementary_ca.frontend;

import jgame.*;

public class Cell extends JGObject
{
	public Cell(int xPos, int yPos, int cid)
	{
		super(
				"cell_black",     // name
				true,             // force unique id
				xPos,             // horizontal position
				yPos,             // vertical position
				cid,              // collision id
				"cell_black"      // graphic
			);
	}
}