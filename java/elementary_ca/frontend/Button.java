package elementary_ca.frontend;

import jgame.*;

public class Button extends JGObject
{
	public final static int WIDTH  = 32;
	public final static int HEIGHT = 24;
	public final static String[] NAMES = { "000", "001", "010", "011", "100", "101", "110", "111", "random" };
	
	private int x;
	private int y;
	private String type;
	private boolean isOn;
	
	public Button(int xPos, int yPos, String name, boolean ison)
	{
		super(
				"button" + name,                            // name
				true,                                       // force unique id
				xPos,                                       // horizontal position
				yPos,                                       // vertical position
				0,                                          // collision id
				"button_" + name + (ison ? "_1" : "_0")     // graphic
			);
		
		this.isOn = ison;
		this.type = name;
		this.x    = xPos;
		this.y    = yPos;
	}
	
	public Button(int xPos, int yPos, String name)
	{
		this(xPos, yPos, name, false);
	}
	
	public boolean isOn() { return isOn; }
	
	public void toggle()
	{
		isOn = !isOn;
		setGraphic("button_" + type + (isOn ? "_1" : "_0"));
	}
	
	public boolean hover(int mouseX, int mouseY)
	{
		return (((mouseX >= x) && (mouseX <= x + WIDTH)) && ((mouseY >= y) && (mouseY <= y + HEIGHT)));
	}
}