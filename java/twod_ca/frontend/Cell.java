package twod_ca.frontend;

import jgame.*;

public class Cell extends JGObject
{
	public final static String[] stdColors = new String[] { "empty", "blue", "white", "red" };
	public final static String[] antColors = new String[] { "empty", "black", "up_white", "up_black", "right_white", "right_black", "down_white", "down_black", "left_white", "left_black" };
	public final static String prefix   = new String("cell");
	
	public final static int WIDTH  = 10;
	public final static int HEIGHT = 10;
	
	private String color;
	private int xpos;
	private int ypos;
	
	public Cell(String typ, int x, int y, int cid)
	{
		super(	prefix + "_" + typ,
				true,
				x,
				y,
				cid,
				prefix + "_" + typ
			);
		
		this.color  = new String(typ);
		this.xpos   = x;
		this.ypos   = y;
	}
	
	public String getColor()   { return this.color;   }
	public int getX()          { return this.xpos;    }
	public int getY()          { return this.ypos;    }
	
	public void setColor(String col)
	{
		color = new String(col);
		updateGraphic();
	}
	
	private void updateGraphic()
	{
		setGraphic(prefix + "_" + color);
	}
}