package twod_ca.frontend;

import jgame.*;

public class Button extends JGObject
{
	public final static String[] gameButtonNames = 
		new String[] { "home", "info", "back", "play", "fore", "save", "load", "clear" };
	public final static String prefix         = new String("button");
	public final static String suffix_passive = new String("0");
	public final static String suffix_active  = new String("1");
	
	public final static int SELECTMODE_WIDTH  = 150;
	public final static int SELECTMODE_HEIGHT =  50;
	public final static int GAMEMODE_WIDTH    =  50;
	public final static int GAMEMODE_HEIGHT   =  50;
	public final static int INFOSCREEN_WIDTH  = 500;
	public final static int INFOSCREEN_HEIGHT = 500;
	public final static int CLOSE_WIDTH       =  70;
	public final static int CLOSE_HEIGHT      =  30;
	
	private int xpos;
	private int ypos;
	private boolean ison;
	private int width;
	private int height;
	private String type;
	
	public Button(String typ, boolean on, int x, int y, int wid, int hig, int cid)
	{
		super(	prefix + "_" + typ,
				true,
				x,
				y,
				cid,
				prefix + "_" + typ + "_" + (on ? suffix_active : suffix_passive)
			);
		
		this.xpos   = x;
		this.ypos   = y;
		this.ison   = on;
		this.width  = wid;
		this.height = hig;
		this.type   = new String(typ);
	}
	
	public String getType() { return this.type; }
	public boolean isOn()   { return this.ison; }
	
	public boolean hover(int mouseX, int mouseY)
	{
		return (	(mouseX >= xpos) && (mouseX <= xpos+width)
				 && (mouseY >= ypos) && (mouseY <= ypos+height)	);
	}
	
	public void toggle()
	{
		ison = !ison;
		updateGraphic();
	}
	
	private void updateGraphic()
	{
		setGraphic(prefix + "_" + type + "_" + (ison ? suffix_active : suffix_passive));
	}
	
	public String toString()
	{
		return ( "(" + Integer.toString(xpos) + "," + Integer.toString(ypos) + ")" + " " + type );
	}
}