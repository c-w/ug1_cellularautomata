package twod_ca;

import jgame.*;
import jgame.platform.*;

import twod_ca.backend.*;
import twod_ca.frontend.*;

public class Main extends JGEngine
{
	// Game relevant stuff
	private Button[] selectModeButtons = new Button[Board.modeNames.length];
	private Button[] gameModeButtons   = new Button[Button.gameButtonNames.length];
	
	private String mode = null;
	private Board board = null;
	private Cell[][] cells = null;
	private int[][][] presetsConway = null;
	private int[][][] presetsWire   = null;
	private int[][][] presetsP2life = null;
	private String[] colors = null;
	
	private Cell mouseFollowingCell = null;
	private Button infoScreen = null;
	
	private int speed = 4;
	private long time;
	private int differentCellTypes;
	private int curCol;
	private boolean showGrid;
	private boolean isPause;
	private boolean playAudio;
	private boolean isInfo;
	
	// Keybinding constants
	public final static int MOUSE_L   = 1;
	public final static int MOUSE_R   = 3;
	public final static int BACKSPACE = 8;
	public final static int CTRL      = 17;
	public final static int ESCAPE    = 27;
	public final static int SPACEBAR  = 32;
	public final static int ZERO      = 48;
	public final static int A         = 65;
	public final static int G         = 71;
	
	// Engine constants
	public final static int FPS       = 35;
	public final static int FRAMESKIP = 2;
	public final static int UI_HEIGHT = 100;
	public final static int WIDTH     = 600;
	public final static int HEIGHT    = 600 + UI_HEIGHT;
	public final static int TILESIZE  = 10;
	public final static int TILES_H   = WIDTH/TILESIZE;
	public final static int TILES_V   = HEIGHT/TILESIZE;
	public final static int FIELD_WIDTH  = WIDTH/TILESIZE;
	public final static int FIELD_HEIGHT = (HEIGHT-UI_HEIGHT)/TILESIZE;
	public final static JGColor FG_COL = new JGColor(51, 153, 51);
	public final static JGColor BG_COL = new JGColor(25,  25, 25);
	public final static JGFont FONT    = new JGFont("monosp", JGFont.BOLD, 250.0);
	
	// Presets/Buffer/external resource constants
	public final static String DOMAIN = new String("http://www.cwinf.altervista.org/twod_ca/src/");
	public final static String DELIM  = new String(",");
	public final static String PREFIX_CONWAY = new String("conway_");
	public final static String PREFIX_WIRE   = new String("wire_");
	public final static String PREFIX_P2LIFE = new String("p2life_");
	public final static int NUM_PRESETS_CONWAY  = 10;
	public final static int FIRST_PRESET_CONWAY = ZERO;
	public final static int LAST_PRESET_CONWAY  = FIRST_PRESET_CONWAY + NUM_PRESETS_CONWAY - 1;
	public final static int NUM_PRESETS_WIRE    = 1;
	public final static int FIRST_PRESET_WIRE   = ZERO;
	public final static int LAST_PRESET_WIRE    = FIRST_PRESET_WIRE   + NUM_PRESETS_WIRE   - 1;
	public final static int NUM_PRESETS_P2LIFE  = 1;
	public final static int FIRST_PRESET_P2LIFE = ZERO;
	public final static int LAST_PRESET_P2LIFE  = FIRST_PRESET_P2LIFE + NUM_PRESETS_P2LIFE - 1;
	public final static int TOTAL_AUDIO_FILES = 12; // audio files by 'pinkyfinger' found at http://www.freesound.org/packsViewSingle.php?id=4409 (creative commons sampling plus 1.0 licence)
	public final static int INFOSCREEN_X = (WIDTH - Button.INFOSCREEN_WIDTH)/2;
	public final static int INFOSCREEN_Y = (HEIGHT - UI_HEIGHT - Button.INFOSCREEN_HEIGHT)/2;
	public final static int INFOSCREENCLOSE_X = INFOSCREEN_X + Button.INFOSCREEN_WIDTH - Button.CLOSE_WIDTH;
	public final static int INFOSCREENCLOSE_Y = INFOSCREEN_Y;
	
	// Collision ids
	private final static int BUTTON_COLID     = 1;
	private final static int CELL_COLID       = 2;
	private final static int INFOSCREEN_COLID = 3;

	// Button constants
	private final static int BUTTONS_PER_LINE    = 3;
	private final static int SELECTMODE_BUTTON_X = (WIDTH - BUTTONS_PER_LINE*Button.SELECTMODE_WIDTH)/(BUTTONS_PER_LINE+1); // equal distance between buttons
	private final static int SELECTMODE_BUTTON_Y = (int)(HEIGHT - (2+Math.floor((double)Board.modeNames.length/(double)BUTTONS_PER_LINE))*Button.SELECTMODE_HEIGHT)/2; // centers vertically
	private final static int GAMEMODE_BUTTON_X = (WIDTH - Button.gameButtonNames.length*Button.GAMEMODE_WIDTH)/(Button.gameButtonNames.length+1);
	private final static int GAMEMODE_BUTTON_Y = HEIGHT - UI_HEIGHT + Button.GAMEMODE_HEIGHT/2; // center vertically in ui footer
	
	
	
	public static void main(String[] args) { new Main(new JGPoint(WIDTH, HEIGHT)); }
	public Main()             { initEngineApplet(); }          // initialize as applet
	public Main(JGPoint size) { initEngine(size.x, size.y); }  // initialize as application
	
	public void initCanvas()
	{
		setCanvasSettings(	TILES_H,  // number of horizontal tiles
							TILES_V,  // number of vertical tiles
							TILESIZE, // tilesize horizontally
							TILESIZE, // tilesize vertically
							FG_COL,   // foreground color (in this class only used during load)
							BG_COL,   // background color (in this class only used during load)
							FONT      // font used by drawString if not specified otherwise
						);
	}
	
	public void initGame()
	{
		setProgressMessage("Two Dimensional Cellular Automata Collection");  // load-message middle-center
		setAuthorMessage("Loading...");                                      // load-message bottom right
		defineMedia("media.tbl"); // pointer to graphic/audio declarations
		setFrameRate(FPS, FRAMESKIP);
		
		 // preset designs are loaded once from host and stored in array for quicker access
		try
		{
		presetsConway = Utils.read2DIntArraysFromWeb(PREFIX_CONWAY, NUM_PRESETS_CONWAY, DOMAIN, DELIM, FIELD_WIDTH, FIELD_HEIGHT);
		presetsWire   = Utils.read2DIntArraysFromWeb(PREFIX_WIRE,   NUM_PRESETS_WIRE,   DOMAIN, DELIM, FIELD_WIDTH, FIELD_HEIGHT);
		presetsP2life = Utils.read2DIntArraysFromWeb(PREFIX_P2LIFE, NUM_PRESETS_P2LIFE, DOMAIN, DELIM, FIELD_WIDTH, FIELD_HEIGHT);
		}
		catch(Exception e)
		{
		}
		
		initNewMode("ModeSelect");
	}
	
	
	
	// 같같같같같같같같같같같� ModeSelect 같같같같같같같같같같같� //
	// displays some hoverable buttons that load individual game modes
	
	public void startModeSelect()
	{
		initSelectModeUI();
	}
	
	public void doFrameModeSelect()
	{
		handleSelectButtonClick();
	}
	
	
	// 같같같같같같같같같같같� ModeWire 같같같같같같같같같같같� //
	// simulates Wireworld (4-state cellular automaton)
	
	public void startModeWire()
	{
		initGameModeUI();
		differentCellTypes = 4;
		curCol = 3;
		board = new Board(mode, differentCellTypes, FIELD_WIDTH, FIELD_HEIGHT);
		board.initPresets(presetsWire);
		colors = Utils.copy(Cell.stdColors);
	}
	
	public void doFrameModeWire()
	{
		if (!isInfo)
		{
			if (isPause)
			{
				handleMouseInput();
				loadPresets(FIRST_PRESET_WIRE, LAST_PRESET_WIRE);
			}
			else
			{
				if (time%speed == 0) { board.nextGen(); }
				checkEmptyOrConstant();
				time++;
			}
			drawBoard(colors);
			handleKeyboardInput();
		}
		else
		{
			handleInfoscreenInput();
		}
		handleGamebuttonInput();
	}
	
	
	
	// 같같같같같같같같같같같� ModeBrian 같같같같같같같같같같같� //
	// simulates Brian's Brain (3-state cellular automaton)
	
	public void startModeBrian()
	{
		initGameModeUI();
		differentCellTypes = 3;
		board = new Board(mode, differentCellTypes, FIELD_WIDTH, FIELD_HEIGHT);
		colors = Utils.copy(Cell.stdColors);
	}
	
	public void doFrameModeBrian()
	{
		if (!isInfo)
		{
			if (isPause)
			{
				handleMouseInput();
			}
			else
			{
				if (time%speed == 0) { board.nextGen(); }
				checkEmptyOrConstant();
				time++;
			}
			toggleAudio();
			drawBoard(colors);
			handleKeyboardInput();
		}
		else
		{
			handleInfoscreenInput();
		}
		handleGamebuttonInput();
	}
	
	
	
	// 같같같같같같같같같같같� ModeP2life 같같같같같같같같같같같� //
	// simulates p2life (2-entity game of life)
	
	public void startModeP2life()
	{
		initGameModeUI();
		differentCellTypes = 3;
		board = new Board(mode, differentCellTypes, FIELD_WIDTH, FIELD_HEIGHT);
		board.initPresets(presetsP2life);
		colors = Utils.copy(Cell.stdColors);
	}
	
	public void doFrameModeP2life()
	{
		if (!isInfo)
		{
			if (isPause)
			{
				handleMouseInput();
				loadPresets(FIRST_PRESET_P2LIFE, LAST_PRESET_P2LIFE);
			}
			else
			{
				if (time%speed == 0) { board.nextGen(); }
				checkEmptyOrConstant();
				time++;
			}
			drawBoard(colors);
			handleKeyboardInput();
		}
		else
		{
			handleInfoscreenInput();
		}
		handleGamebuttonInput();
	}
	
	
	
	// 같같같같같같같같같같같� ModeConway 같같같같같같같같같같같� //
	// simulates conway's game of life
	
	public void startModeConway()
	{
		initGameModeUI();
		differentCellTypes = 2;
		board = new Board(mode, differentCellTypes, FIELD_WIDTH, FIELD_HEIGHT);
		board.initPresets(presetsConway);
		colors = Utils.copy(Cell.stdColors);
	}
	
	public void doFrameModeConway()
	{
		if (!isInfo)
		{
			if (isPause)
			{
				handleMouseInput();
				loadPresets(FIRST_PRESET_CONWAY, LAST_PRESET_CONWAY);
			}
			else
			{
				if (time%speed == 0) { board.nextGen(); }
				checkEmptyOrConstant();
				time++;
			}
			toggleAudio();
			drawBoard(colors);
			handleKeyboardInput();
		}
		else
		{
			handleInfoscreenInput();
		}
		handleGamebuttonInput();
	}
	
	
	
	// 같같같같같같같같같같같� ModeLangton 같같같같같같같같같같같� //
	// simulates langton's ant
	
	public void startModeLangton()
	{
		initGameModeUI();
		differentCellTypes = 10;
		board = new Board(mode, differentCellTypes, FIELD_WIDTH, FIELD_HEIGHT);
		colors = Utils.copy(Cell.antColors);
	}
	
	public void doFrameModeLangton()
	{
		if (!isInfo)
		{
			if (isPause)
			{
				handleMouseInput();
			}
			else
			{
				board.nextGen();
				if (time%speed == 0) { checkEmptyOrConstant(); }
				time++;
			}
			drawBoard(colors);
			handleKeyboardInput();
		}
		else
		{
			handleInfoscreenInput();
		}
		handleGamebuttonInput();
	}
	
	
	
	// 같같같같같같같같같같같� Auxiliary functions 같같같같같같같같같같같� //
	
	private void drawBoard(String[] cols)
	{
		int x; int y;
		int[][] gen = board.getBoard();
		
		for (y = 0; y < gen.length; y++)
		{
			for (x = 0; x < gen[y].length; x++)
			{
				if (gen[y][x] == Board.EMPTY)
				{
					if (cells[y][x] != null)
					{
						removeObject(cells[y][x]);
						cells[y][x] = null;
					}
				}
				else
				{
					if (cells[y][x] == null)
					{
						cells[y][x] = new Cell(cols[gen[y][x]], x*TILESIZE, y*TILESIZE, CELL_COLID);
						
						if (playAudio && (((time%(x+1) == 0) && (time%(y+1) == 0) && (gen[y][x] == 1)) || isPause)) // not the most expressive algorithm (bias towards cells in lower right hand corner) but produces 'nicer' tunes than alternatives (eg. number of neighbours, etc.)
						{
							playAudio("piano_" + Integer.toString((x+y)%TOTAL_AUDIO_FILES));
						}
					}
					else if (!cells[y][x].getColor().equals(cols[gen[y][x]]))
					{
						cells[y][x].setColor(cols[gen[y][x]]);
					}
				}
			}
		}
	}
	
	private void handleInfoscreenInput()
	{
		if (infoScreen.hover(getMouseX(), getMouseY()))
		{
			if (!infoScreen.isOn()) { infoScreen.toggle(); }
			
			if (getMouseButton(MOUSE_L))
			{
				clearMouseButton(MOUSE_L);
				toggleInfoScreen(mode);
			}
		}
		else
		{
			if (infoScreen.isOn()) { infoScreen.toggle(); }
		}
	}
	
	private void handleKeyboardInput()
	{
		if (getKey(ESCAPE))
		{
			clearKey(ESCAPE);
			initNewMode("ModeSelect");
		}
		else if (getKey(SPACEBAR))
		{
			clearKey(SPACEBAR);
			togglePause();
		}
		else if (getKey(G))
		{
			clearKey(G);
			showGrid = !showGrid;
			updateBGImage();
		}
		else if (isPause)
		{
			if (getKey(BACKSPACE))
			{
				board.init();
			}
		}
	}
	
	private void handleMouseInput()
	{
		int x = getMouseX();
		int y = getMouseY();
		
		if (getMouseButton(MOUSE_R))
		{
			clearMouseButton(MOUSE_R);
			switchCellColor(differentCellTypes);
		}
		
		if (Utils.isInBounds(x, y, WIDTH, HEIGHT-UI_HEIGHT))
		{
			int normX = x/TILESIZE; // normalize to [0;60[
			int normY = y/TILESIZE; // normalize to [0;60[
			int gridX = Utils.discardUnits(x);  // snap to grid
			int gridY = Utils.discardUnits(y);  // snap to grid
			
			// handle mousefollowing cell
			if (board.isEmpty(normX, normY))
			{
				if (mouseFollowingCell == null)
				{
					mouseFollowingCell = new Cell(colors[curCol], gridX, gridY, CELL_COLID);
				}
				else
				{
					mouseFollowingCell.x = gridX;
					mouseFollowingCell.y = gridY;
					
					if (!mouseFollowingCell.getColor().equals(colors[curCol]))
					{
						mouseFollowingCell.setColor(colors[curCol]);
					}
				}
			}
			else if (mouseFollowingCell != null)
			{
				removeObject(mouseFollowingCell);
				mouseFollowingCell = null;
			}
			
			// handle clicks
			if (getMouseButton(MOUSE_L))
			{
				boolean draw = true;
				
				if (!getKey(CTRL))  // hold ctrl to enable drawing/spraying
				{
					draw = false;
					clearMouseButton(MOUSE_L);
				}
				
				if (draw || board.isEmpty(normX, normY))
				{
					board.add(normX, normY, curCol);
				}
				else
				{
					board.remove(normX, normY);
				}
			}
		}
		else
		{
			if (mouseFollowingCell != null)
			{
				removeObject(mouseFollowingCell);
				mouseFollowingCell = null;
			}
		}
	}
	
	private void handleGamebuttonInput()
	{
		Button b;
		
		if ((b = hoverButtons(gameModeButtons)) != null)
		{
			String name = b.getName();
			
			if (name.contains("home"))
			{
				initNewMode("ModeSelect");
			}
			else if (name.contains("info"))
			{
				toggleInfoScreen(mode);
			}
			else if (!isInfo)
			{
				if (name.contains("pause"))
				{
					togglePause();
				}
				else if (name.contains("save"))
				{
					board.saveCurrentBoard();
				}
				else if (name.contains("clear"))
				{
					board.init();
				}
				else if (isPause)
				{
					if (name.contains("back"))
					{
						board.stepBack();
					}
					else if (name.contains("play"))
					{
						togglePause();
					}
					else if (name.contains("fore"))
					{
						board.stepFore();
					}
					else if (name.contains("load"))
					{
						board.loadSavedBoard();
					}
				}
			}
		}
	}
	
	private void toggleInfoScreen(String type)
	{
		isInfo = !isInfo;
		
		if (isInfo)
		{
			removeObjects(null, CELL_COLID);
			cells = new Cell[FIELD_HEIGHT][FIELD_WIDTH];
			infoScreen = new Button("info_" + type, false, INFOSCREEN_X, INFOSCREEN_Y, Button.INFOSCREEN_WIDTH, Button.INFOSCREEN_HEIGHT, INFOSCREEN_COLID);
		}
		else
		{
			removeObject(infoScreen);
			infoScreen = null;
			drawBoard(colors);
		}
	}
	
	private void checkEmptyOrConstant() { if (board.isEmpty() || board.isConstant()) { togglePause(); } }
	
	
	private void togglePause()
	{
		isPause = !isPause;
		if (mouseFollowingCell != null)
		{
			removeObject(mouseFollowingCell);
			mouseFollowingCell = null;
		}
		
		int i = Utils.getIndexOf("play", Button.gameButtonNames);
		removeObject(gameModeButtons[i]);   // switch play/pause button
		gameModeButtons[i] = new Button(	isPause ? "play" : "pause",
											false,
											GAMEMODE_BUTTON_X*(i+1) + Button.GAMEMODE_WIDTH*(i),
											GAMEMODE_BUTTON_Y,
											Button.GAMEMODE_WIDTH,
											Button.GAMEMODE_HEIGHT,
											BUTTON_COLID	);
	}
	
	private void initNewMode(String modeName) // does some cleanup, loads background image, switches state
	{
		removeObjects(null, 0);    // cleanup
		mouseFollowingCell = null; // cleanup
		infoScreen         = null; // cleanup
		colors             = null; // cleanup
		
		time      = 0L;                              // set default
		showGrid  = false;                           // set default
		isPause   = true;                            // set default
		isInfo    = false;                           // set default
		playAudio = false;                           // set default
		curCol    = 1;                               // set default
		differentCellTypes = 0;                      // set default
		cells = new Cell[FIELD_HEIGHT][FIELD_WIDTH]; // set default
		setMouseCursor(DEFAULT_CURSOR);              // set default
		
		mode = new String(modeName); // update state
		updateBGImage();             // update state
		setGameState(mode);          // switch state
	}
	
	private void updateBGImage() { setBGImage("bg_" + mode + (showGrid ? "_grid" : "")); }
	
	private void switchCellColor(int max) { curCol = (curCol+1 >= max) ? 1 : curCol+1; }
	
	private void initGameModeUI()
	{
		int i;
		
		for (i = 0; i < gameModeButtons.length; i++)
		{
			gameModeButtons[i] = new Button(	Button.gameButtonNames[i],
												false,
												GAMEMODE_BUTTON_X*(i+1) + Button.GAMEMODE_WIDTH*(i),
												GAMEMODE_BUTTON_Y,
												Button.GAMEMODE_WIDTH,
												Button.GAMEMODE_HEIGHT,
												BUTTON_COLID	);
		}
	}
	
	private Button hoverButtons(Button[] buttons)
	{
		for (Button b : buttons)
		{
			if (b.hover(getMouseX(), getMouseY()))
			{
				if (!b.isOn())
				{
					b.toggle();
					return null;
				}
				else
				{
					if (getMouseButton(MOUSE_L))
					{
						clearMouseButton(MOUSE_L);
						return b;
					}
				}
			}
			else
			{
				if (b.isOn())
				{
					b.toggle();
					return null;
				}
			}
		}
		return null;
	}
	
	private void loadPresets(int first, int last)
	{
		int i;
		
		for (i = first; i <= last; i++)
		{
			if (getKey(i))
			{
				clearKey(i);
				board.loadPreset(i-'0');
			}
		}
	}
	
	private void toggleAudio()
	{
		if (getKey(A))
		{
			clearKey(A);
			playAudio = !playAudio;
		}
	}
	
	private void handleSelectButtonClick()
	{
		Button b;
		
		if ((b = hoverButtons(selectModeButtons)) != null)
		{
			initNewMode(b.getType());
		}
	}
	
	private void initSelectModeUI()
	{
		int i; int x; int y;
		
		for (i = 0, x = 0, y = 0; i < selectModeButtons.length; i++, x++)
		{
			if (x > BUTTONS_PER_LINE-1) { x = 0; y++; }
			
			selectModeButtons[i] = new Button(	Board.modeNames[i],
												false,
												SELECTMODE_BUTTON_X*(x+1) + Button.SELECTMODE_WIDTH*(x),
												SELECTMODE_BUTTON_Y       + Button.SELECTMODE_HEIGHT*(2*y),
												Button.SELECTMODE_WIDTH,
												Button.SELECTMODE_HEIGHT,
												BUTTON_COLID	);
		}
	}
}