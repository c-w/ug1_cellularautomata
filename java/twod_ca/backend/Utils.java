package twod_ca.backend;

import java.io.*;
import java.net.*;
import java.util.Arrays;

public class Utils
{
	public static int[][][] read2DIntArraysFromWeb(String prefix, int num, String domain, String delim, int width, int height)
	{
		int i;
		int[][][] pre = new int[num][][];
		
		for (i = 0; i < pre.length; i++)
		{
			pre[i] = read2DIntArrayFromWeb(domain + prefix + Integer.toString(i) + ".txt", delim, width, height);
		}
		
		return pre;
	}
	
	public static int[][] read2DIntArrayFromWeb(String path, String delim, int width, int height)
	{
		int x; int y;
		String line;
		
		URL url = null;
		URLConnection urlConn = null;
		InputStreamReader inStream = null;
		BufferedReader buff = null;
		
		int[][] a = new int[height][width];
		
		try
		{	
			url = new URL(path);
			urlConn = url.openConnection();
			inStream = new InputStreamReader(urlConn.getInputStream());
			buff = new BufferedReader(inStream);
			
			for (y = 0; y < a.length; y++)
			{
				line = buff.readLine();
				if (line == null) { return null; }
				
				String[] nums = line.split(delim);
				
				for (x = 0; x < a[y].length; x++)
				{
					a[y][x] = Integer.parseInt(nums[x]);
				}
			}
		}
		catch (MalformedURLException e)
		{
			System.out.printf("FileReader.read() - bad url: " + e.toString());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (buff     != null) { buff.close(); }
				if (inStream != null) { inStream.close(); }
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		
		return a;
	}
	
	public static int getIndexOf(String elem, String[] arr) // returns -1 or index of elem if it is in arr
	{
		return Arrays.asList(arr).indexOf(elem);
	}
	
	public static int get2DIntArrayNeighbourToroidal(int x, int y, int xOffset, int yOffset, int[][] a)
	{
		int neighbourX = x + xOffset;
		int neighbourY = y + yOffset;
		
		if      (neighbourX < 0)             { neighbourX += a[0].length; }   // wrap left->right
		else if (neighbourX > a[0].length-1) { neighbourX -= a[0].length; }   // wrap right->left
		
		if      (neighbourY < 0)             { neighbourY += a.length; }      // wrap top->bottom
		else if (neighbourY > a.length-1)    { neighbourY -= a.length; }      // wrap bottom->top
		
		return a[neighbourY][neighbourX];
	}
	
	public static int discardUnits(int x)
	{
		while (x%10 != 0) { x--; }
		return x;
	}
	
	public static boolean isInBounds(int x, int y, int width, int height)
	{
		return (	(x >= 0) && (x < width)
				 && (y >= 0) && (y < height)	);
	}
	
	public static boolean areEqual(int[][] a, int[][] b)
	{
		int i; int j;
		
		if (a.length != b.length) { return false; }
		
		for (i = 0; i < a.length; i++)
		{
			if (a[i].length != b[i].length) { return false; }
			
			for (j = 0; j < a[i].length; j++)
			{
				if (a[i][j] != b[i][j]) { return false; }
			}
		}
		
		return true;
	}
	
	public static void setAll(int num, int[][] a)
	{
		int x; int y;
		
		for (y = 0; y < a.length; y++)
		{
			for (x = 0; x < a[y].length; x++)
			{
				a[y][x] = num;
			}
		}
		
		return;
	}
	
	public static boolean allElementsZero(int[][] a)
	{
		int x; int y;
		
		for (y = 0; y < a.length; y++)
		{
			for (x = 0; x < a[y].length; x++)
			{
				if (a[y][x] != 0) { return false;  }
			}
		}
		return true;
	}
	
	public static String[] copy(String[] a) // return a deep copy of a
	{
		return (String[]) a.clone();
	}
	
	public static int[][][] copy(int[][][] a) // return a deep copy of a
	{
		return (int[][][]) a.clone();
	}
	
	public static int[][] copy(int[][] a) // return a deep copy of a
	{
		return (int[][]) a.clone();
	}
}