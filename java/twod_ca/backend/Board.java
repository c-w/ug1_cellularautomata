package twod_ca.backend;

import java.util.ArrayList;

import twod_ca.backend.Utils;

public class Board
{
	public final static int EMPTY = 0;
	public final static int BLUE  = 1;
	public final static int WHITE = 2;
	public final static int RED   = 3;
	public final static String[] modeNames = 
		new String[] { "ModeConway", "ModeP2life", "ModeBrian", "ModeWire", "ModeLangton" };
	
	private int[][] int_arr;              // current board state
	private final int width;              // int_arr[0].length
	private final int height;             // int_arr.length
	private int[][] savedBoard;           // board saved by user
	private int[][][] presets;            // preset designs are stored here afer being passed to instance
	private ArrayList<int[][]> buffer;    // save each board to be able to step back/forewards
	int bufferPos;                        // current position in buffer
	private String mode;                  // type of board simulated
	private int[] neighbours;             // holds counts of types of neighbours (eg neighbours[0] #empty)
	
	public Board(String m, int diffCellTypes, int w, int h)
	{
		mode       = new String(m);
		width      = w;
		height     = h;
		int_arr    = new int[height][width];
		savedBoard = new int[height][width];
		neighbours = new int[diffCellTypes];
		buffer     = new ArrayList<int[][]>();
		bufferPos  = 0;
	}
	
	public int[][] getBoard() { return int_arr; }
	
	public void nextGen()
	{
		buffer.add(int_arr); bufferPos = buffer.size(); // store old state
		int_arr = iterate(int_arr);                     // get new state
	}
	
	public void init()
	{
		int_arr = new int[height][width];
		initBuffer();
	}
	
	public void stepBack()
	{
		if (bufferPos > 0) { int_arr = buffer.get(--bufferPos); }
	}
	
	public void stepFore()
	{
		if (bufferPos < buffer.size()) { int_arr = buffer.get(bufferPos++); }
		else                           { nextGen(); }
	}
	
	public void saveCurrentBoard()
	{
		savedBoard = Utils.copy(int_arr);
	}
	
	public void loadSavedBoard()
	{
		if (!Utils.allElementsZero(savedBoard))
		{
			initBuffer();
			int_arr = Utils.copy(savedBoard);
		}
	}
	
	public void initPresets(int[][][] p) { presets = Utils.copy(p); }
	
	public void loadPreset(int num)
	{
		initBuffer();
		int_arr = presets[num];
	}
	
	public boolean isConstant()
	{
		if (bufferPos < 2) { return false; }
		
		return Utils.areEqual(buffer.get(bufferPos-1), buffer.get(bufferPos-2));
	}
	
	public boolean isEmpty() { return Utils.allElementsZero(int_arr); }
	
	public boolean isEmpty(int mouseX, int mouseY)
	{
		if (Utils.isInBounds(mouseX, mouseY, width, height))
		{
			return (int_arr[mouseY][mouseX] == EMPTY);
		}
		return false;
	}
	
	public boolean add(int mouseX, int mouseY, int color)
	{
		if (Utils.isInBounds(mouseX, mouseY, width, height))
		{
			int_arr[mouseY][mouseX] = color;
			return true;
		}
		return false;
	}
	
	public boolean remove(int mouseX, int mouseY)
	{
		if (Utils.isInBounds(mouseX, mouseY, width, height))
		{
			if (int_arr[mouseY][mouseX] == EMPTY) { return false; }
			
			int_arr[mouseY][mouseX] = EMPTY;
			return true;
		}
		return false;
	}
	
	public void initBuffer()
	{
		buffer = new ArrayList<int[][]>();
		bufferPos = 0;
	}
	
	private int[][] iterate(int[][] old)
	{
		if      (mode.equals(modeNames[0])) { return iterateConway(old);  }     // Conway's Game of Life
		else if (mode.equals(modeNames[1])) { return iterateP2life(old);  }     // P2life
		else if (mode.equals(modeNames[2])) { return iterateBrian(old);   }     // Brian's Brain
		else if (mode.equals(modeNames[3])) { return iterateWire(old);    }     // WireWorld
		else if (mode.equals(modeNames[4])) { return iterateLangton(old); }     // Langton's Ant
		
		return null;
	}
	
	private int[][] iterateLangton(int[][] old)
	{
		ArrayList<Integer> processed = new ArrayList<Integer>();
		int x; int y;
		int[][] nextGen = new int[old.length][old[0].length];
		int lastGen;
		int nextAntDir;
		int nextY;
		int nextX;
		int maxCols = neighbours.length;
		
		for (y = 0; y < nextGen.length; y++)
		{
			for (x = 0; x < nextGen[y].length; x++)
			{
				if (processed.contains(y*1000 + x)) { continue; }
				
				lastGen = old[y][x];
				
				if (lastGen <= 1)                          // black or white cell (keep it)
				{
					nextGen[y][x] = lastGen;
				}
				else                                       // ant cell:
				{
					nextGen[y][x] = 1-lastGen%2;           // ...switch color of old cell
					
					if (lastGen%2 == 0)                    // ...old cell was white -> turn left
					{
						nextAntDir = (lastGen+2 >= maxCols) ? 2+(lastGen+2)%maxCols : lastGen+2;
					}
					else                                   // ...old cell was black -> turn right
					{
						nextAntDir = (lastGen-2 <= 1) ? maxCols-1-(3-lastGen) : lastGen-2;
					}
					
					if ((nextAntDir == 2) || (nextAntDir == 3)) // up
					{
						nextY = (y-1 < 0) ? height-1 : y-1;
						nextX = x;
					}
					else if ((nextAntDir == 6) || (nextAntDir == 7)) // down
					{
						nextY = (y+1 >= height) ? 0 : y+1;
						nextX = x;
					}
					else if ((nextAntDir == 4) || (nextAntDir == 5)) // right
					{
						nextY = y;
						nextX = (x+1 >= width) ? 0 : x+1;
					}
					else if ((nextAntDir == 8) || (nextAntDir == 9)) // left
					{
						nextY = y;
						nextX = (x-1 < 0) ? width-1 : x-1;
					}
					else
					{
						return null;
					}
					
					if (old[nextY][nextX]%2 == 0)     // target cell is white, adapt color of ant
					{
						nextAntDir = (nextAntDir%2 == 0) ? nextAntDir : nextAntDir-1; 
					}
					else                              // target cell is black, adapt color of ant
					{
						nextAntDir = (nextAntDir%2 == 1) ? nextAntDir : nextAntDir+1;
					}
					
					nextGen[nextY][nextX] = nextAntDir;  //...move forward one unit
					processed.add(nextY*1000 + nextX);   // make sure cell is not overwritten
				}
			}
		}
		
		return nextGen;
	}
	
	private int[][] iterateConway(int[][] old)
	{
		int x; int y;
		int[][] nextGen = new int[old.length][old[0].length];
		int lastGen;
		
		for (y = 0; y < nextGen.length; y++)
		{
			for (x = 0; x < nextGen[y].length; x++)
			{
				lastGen = old[y][x];
				countNeighbours(x, y, old); // neighbours[] set
				
				if (lastGen == EMPTY)
				{
					if (neighbours[EMPTY] == 8) // nothing can happen here - skip for better performance
					{
						continue;
					}
					if (neighbours[BLUE] == 3)                                   // reproduction
					{                                                            // new cell is created
						nextGen[y][x] = BLUE;
					}
				}
				else if (lastGen == BLUE)
				{
					if (neighbours[BLUE] < 2)                                    // underpopulation
					{                                                            // cell dies
						nextGen[y][x] = EMPTY;
					}
					else if ((neighbours[BLUE] == 2) || (neighbours[BLUE] == 3)) // survival
					{                                                            // cell lives on
						nextGen[y][x] = BLUE;
					}
					else if (neighbours[BLUE] > 3)                               // overpopulation
					{                                                            // cell dies
						nextGen[y][x] = EMPTY;
					}
				}
			}
		}
		
		return nextGen;
	}
	
	private int[][] iterateP2life(int[][] old)
	{
		int x; int y;
		int[][] nextGen = new int[old.length][old[0].length];
		int lastGen;
		int neighbourDifference;
		
		for (y = 0; y < nextGen.length; y++)
		{
			for (x = 0; x < nextGen[y].length; x++)
			{
				lastGen = old[y][x];
				countNeighbours(x, y, old); // neighbours[] set
				
				if (lastGen == EMPTY)
				{
					if (neighbours[EMPTY] == 8) // nothing can happen here - skip for better performance
					{
						continue;
					}
					else if ((neighbours[BLUE] == 3) && (neighbours[WHITE] != 3))   // blue majority birth
					{
						nextGen[y][x] = BLUE;
					}
					else if ((neighbours[WHITE] == 3) && (neighbours[BLUE] != 3))   // white majority birth
					{
						nextGen[y][x] = WHITE;
					}
					else if ((neighbours[BLUE] == 3) && (neighbours[WHITE] == 3))   // random birth
					{
						nextGen[y][x] = (Math.random() < 0.5) ? BLUE : WHITE;
					}
				}
				else if (lastGen == BLUE)
				{
					neighbourDifference = neighbours[BLUE]-neighbours[WHITE];
					
					if ((neighbourDifference == 2) || (neighbourDifference == 3))   // blue survival
					{
						nextGen[y][x] = BLUE;
					}
					else if ((neighbourDifference == 1) && (neighbours[BLUE] >= 2))  // blue survival
					{
						nextGen[y][x] = BLUE;
					}
					else
					{
						nextGen[y][x] = EMPTY;
					}
				}
				else if (lastGen == WHITE)
				{
					neighbourDifference = neighbours[WHITE]-neighbours[BLUE];
					
					if ((neighbourDifference == 2) || (neighbourDifference == 3))    // white survival
					{
						nextGen[y][x] = WHITE;
					}
					else if ((neighbourDifference == 1) && (neighbours[WHITE] >= 2)) // white survival
					{
						nextGen[y][x] = WHITE;
					}
					else
					{
						nextGen[y][x] = EMPTY;
					}
				}
			}
		}
		
		return nextGen;
	}
	
	private int[][] iterateBrian(int[][] old)
	{
		int x; int y;
		int[][] nextGen = new int[old.length][old[0].length];
		int lastGen;
		
		for (y = 0; y < nextGen.length; y++)
		{
			for (x = 0; x < nextGen[y].length; x++)
			{
				lastGen = old[y][x];
				countNeighbours(x, y, old); // neighbours[] set
				
				if (lastGen == EMPTY)
				{
					if (neighbours[EMPTY] == 8) // nothing can happen here - skip for better performance
					{
						continue;
					}
					else if (neighbours[BLUE] == 2)   // reproduction
					{
						nextGen[y][x] = BLUE;
					}
				}
				else if (lastGen == BLUE)            // live cells die
				{
					nextGen[y][x] = WHITE;
				}
				else if (lastGen == WHITE)            // dead cells are removed
				{
					nextGen[y][x] = EMPTY;
				}
			}
		}
		
		return nextGen;
	}
	
	private int[][] iterateWire(int[][] old)
	{
		int x; int y;
		int[][] nextGen = new int[old.length][old[0].length];
		int lastGen;
		
		for (y = 0; y < nextGen.length; y++)
		{
			for (x = 0; x < nextGen[y].length; x++)
			{
				lastGen = old[y][x];
				countNeighbours(x, y, old); // neighbours[] set
				
				if (lastGen == EMPTY)
				{
					continue;
				}
				else if (lastGen == BLUE)             // electron head -> electron tail
				{
					nextGen[y][x] = WHITE;
				}
				else if (lastGen == WHITE)            // electron tail -> conductor
				{
					nextGen[y][x] = RED;
				}
				else if (lastGen == RED)
				{
					if ((neighbours[BLUE] == 1) || (neighbours[BLUE] == 2))
					{
						nextGen[y][x] = BLUE;         // conductor -> electron head
					}
					else
					{
						nextGen[y][x] = RED;          // stay conductor
					}
				}
			}
		}
		
		return nextGen;
	}
	
	private int countNeighbours(int x, int y, int[][] a)
	{
		int i; int count;
		neighbours = new int[neighbours.length]; // reset counts
		
		neighbours[Utils.get2DIntArrayNeighbourToroidal(x, y, -1, -1, a)]++;      // top left
		neighbours[Utils.get2DIntArrayNeighbourToroidal(x, y, -1,  0, a)]++;      // top
		neighbours[Utils.get2DIntArrayNeighbourToroidal(x, y, -1,  1, a)]++;      // top right
		neighbours[Utils.get2DIntArrayNeighbourToroidal(x, y,  0, -1, a)]++;      // left
		neighbours[Utils.get2DIntArrayNeighbourToroidal(x, y,  0,  1, a)]++;      // right
		neighbours[Utils.get2DIntArrayNeighbourToroidal(x, y,  1, -1, a)]++;      // bottom left
		neighbours[Utils.get2DIntArrayNeighbourToroidal(x, y,  1,  0, a)]++;      // bottom
		neighbours[Utils.get2DIntArrayNeighbourToroidal(x, y,  1,  1, a)]++;      // bottom right
		
		for (i = 1, count = 0; i < neighbours.length; i++) // skip first entry ie. empty neighbours
		{
			count += neighbours[i];
		}
		return count;
	}
	
	public String toString()
	{
		int x; int y;
		String s = new String();
		
		for (y = 0; y < height; y++)
		{
			for (x = 0; x < width; x++)
			{
				s += Integer.toString(int_arr[y][x]) + ((x == width-1) ? "" : ", ");
			}
			s += "\n";
		}
		
		return s;
	}
}