Cellular Automata Collection

Java implementations use JGame for GUI.
Python implementations use PyGame for GUI.

1) java/twod_ca implements:
1.a) Conway's Game of Life (GoL) - the classic cellular automaton.
     Bonus feature: (very) naively synthesise music based on cells being born.
1.b) p2life - GoL with two agents.
1.c) Brian's Brain - GoL with three states.
1.d) Wireworld - a cellular automaton that can simulate logic gates nicely.
1.e) Langton's Ant - Turing machine with interesting emergent behaviour
     implemented as cellular automaton.

2) java/elementary_ca implements all single-1 history cellular automata.

3) python/temp_ca is a sample application of cellular automata: it simulates hot
   and cold sinks.
